<?php

namespace App\Domain\Repository;

use App\Domain\Model\User;
use ReflectionException;

class UserRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(User::class);
  }
}
