<?php

namespace App\Utility;

class StringUtility
{
  /**
   * Converts a camelCase string like 'firstName' to snake_case like 'first_name'.
   *
   * @param string $string
   * @return string
   */
  public static function camelCaseToSnakeCase(string $string): string
  {
    $string[0] = strtolower($string[0]);
    return preg_replace_callback('/([A-Z])/', function ($matches) {
      return '_' . strtolower($matches[1]);
    }, $string);
  }

  /**
   * Converts a snake_case string like 'first_name' to camelCase like 'firstName'.
   *
   * @param $string
   * @return string
   */
  public static function snakeCaseToCamelCase(string $string): string
  {
    return str_replace(' ', '', ucwords(str_replace('_', ' ', $string)));
  }
}
