<?php

namespace App\Domain\Model;

class AttributeType extends AbstractModel
{
  /**
   * @var array
   */
  public static $saveFields = ['nameEn', 'nameDe', 'unit'];

  /**
   * @var string
   */
  protected $nameEn;

  /**
   * @var string
   */
  protected $nameDe; 

  /**
   * @var string
   */
  protected $unit;

  /**
   * @param $nameEn string
   */
  public function setNameEn($nameEn): void
  {
    $this->nameEn = $nameEn;
  }

  /**
   * @return string
   */
  public function getNameEn(): string
  {
    return $this->nameEn;
  }

  /**
   * @param $nameDe string
   */
  public function setNameDe($nameDe): void
  {
    $this->nameDe = $nameDe;
  }

  /**
   * @return string
   */
  public function getNameDe(): string
  {
    return $this->nameDe;
  }

  /**
   * @param $unit string
   */
  public function setUnit($unit): void
  {
    $this->unit = $unit;
  }

  /**
   * @return string
   */
  public function getUnit(): string
  {
    return $this->unit;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'name_en' => $this->nameEn,
        'name_de' => $this->nameDe,
        'unit' => $this->unit
      ]
    );
  }
}
