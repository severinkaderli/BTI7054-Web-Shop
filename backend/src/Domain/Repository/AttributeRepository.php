<?php

namespace App\Domain\Repository;

use App\Domain\Model\Attribute;
use ReflectionException;

class AttributeRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(Attribute::class);
  }
}
