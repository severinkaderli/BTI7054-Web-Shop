# Planning
## Components
- Product Panel
- Product Listing

## Pages
- Product Page
- Search Page
- Category Page
- Checkout Page
- Shopping Cart Page
- Current Offers
- Product Management Page
- Category Management Page
