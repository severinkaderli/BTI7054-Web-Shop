<?php

namespace App\Domain\Model;

use App\Domain\Repository\CategoryRepository;
use App\Domain\Repository\RatingRepository;
use App\Domain\Repository\AttributeRepository;
use ReflectionException;

class Product extends AbstractModel
{
  /**
   * @var array
   */
  public static $saveFields = ['name', 'price', 'isOffer', 'category', 'img'];

  /**
   * @var string
   */
  private $name;

  /**
   * The price of the product in the smallest unit.
   *
   * @var int
   */
  private $price;

  /**
   * Whether the product is a offer.
   *
   * @var bool
   */
  private $isOffer;

  /**
   * @var img
   */
  private $img;

  /**
   * @var Category
   */
  private $category;

  /**
   * @return string
   */
  public function getName(): string
  {
    return $this->name;
  }

  /**
   * @param string $name
   */
  public function setName(string $name): void
  {
    $this->name = $name;
  }

  /**
   * @return int
   */
  public function getPrice(): int
  {
    return $this->price;
  }

  /**
   * @param int $price
   */
  public function setPrice(int $price): void
  {
    $this->price = $price;
  }

  /**
   * @return bool
   */
  public function isOffer(): bool
  {
    return $this->isOffer;
  }

  /**
   * @param bool $isOffer
   */
  public function setIsOffer(bool $isOffer): void
  {
    $this->isOffer = $isOffer;
  }

  /**
   * @return string
   */
  public function getImg(): string {
    return $this->img;
  }

  /**
   * @param string $img
   */
  public function setImg(string $img): void {
    $this->img = $img;
  }

  /**
   * @return Category
   */
  public function getCategory(): Category
  {
    return $this->category;
  }

  /**
   * @param int|Category $category
   * @throws ReflectionException
   */
  public function setCategory($category): void
  {
    if (is_numeric($category)) {
      $categoryRepository = new CategoryRepository();
      $category = $categoryRepository->findById($category);
    }

    $this->category = $category;
  }

  /**
   * @return int
   */
  public function getRating(): int
  {
    $ratingRepository = new RatingRepository();
    return $ratingRepository->getRatingByProductId($this->getId());
  }

  public function getAttributes(): array
  {
    $attributeRepository = new AttributeRepository();
    return $attributeRepository->findAllBy('product', $this->getId());
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'name' => $this->name,
        'price' => $this->price,
        'isOffer' => $this->isOffer,
        'img' => $this->img,
        'category' => $this->category,
        'rating' => $this->getRating(),
        'attributes' => $this->getAttributes()
      ]
    );
  }
}
