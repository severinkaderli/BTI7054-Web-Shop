<?php


namespace App\Application\Controller;


use App\Domain\Repository\AbstractRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

abstract class AbstractController
{
  /**
   * @var AbstractRepository
   */
  protected $repository;

  /**
   * @param string $repository
   */
  protected function __construct(string $repository = AbstractRepository::class)
  {
    $this->repository = new $repository();
  }

  /**
   * Returns a json object as a response.
   *
   * @param Response $response
   * @param $data
   * @return Response
   */
  protected function jsonResponse(Response $response, $data)
  {
    if (!$data) {
      $data = [];
    }

    $response = $response->withHeader('Content-Type', 'application/json');
    $response->getBody()->write(json_encode($data));
    return $response;
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function index(Request $request, Response $response, array $arguments = [])
  {
    return $this->jsonResponse($response, $this->repository->findAll());
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function get(Request $request, Response $response, array $arguments = [])
  {
    return $this->jsonResponse($response, $this->repository->findById(intval($arguments['id'])));
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function new(Request $request, Response $response, array $arguments = [])
  {
    $parameters = json_decode($request->getBody()->getContents());
    $modelName = $this->repository->getModel();
    $model = new $modelName((array)$parameters);
    return $this->jsonResponse($response, $this->repository->save($model));
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function update(Request $request, Response $response, array $arguments = [])
  {
    $parameters = json_decode($request->getBody()->getContents());
    $model = $this->repository->findById($arguments['id']);
    $model->fill((array)$parameters);
    return $this->jsonResponse($response, $this->repository->update($model));
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function delete(Request $request, Response $response, array $arguments = [])
  {
    $model = $this->repository->findById($arguments['id']);
    $this->repository->delete($model);
    return $this->jsonResponse($response, []);
  }
}
