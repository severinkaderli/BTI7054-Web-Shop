<?php

namespace App\Application\Controller;

use App\Domain\Repository\UserRepository;
use Dotenv\Dotenv;
use Firebase\JWT\JWT;
use InvalidArgumentException;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class UserController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(UserRepository::class);
  }

  public function login(Request $request, Response $response, array $arguments = [])
  {
    $arguments = json_decode($request->getBody()->getContents());
    $dotenv = Dotenv::create(dirname(dirname(dirname(__DIR__))));
    $dotenv->load();
    $user = $this->repository->findBy('username', $arguments->username);

    if (!$user) {
      throw new InvalidArgumentException('Username or password is invalid.');
    }

    if (!password_verify($arguments->password, $user->getPassword())) {
      throw new InvalidArgumentException('Username or password is invalid.');
    }

    $token = JWT::encode(['user' => $user], getenv('JWT_SECRET'), "HS256");
    return $this->jsonResponse($response, ['token' => $token]);
  }
}
