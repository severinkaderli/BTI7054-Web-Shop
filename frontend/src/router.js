import Vue from "vue";
import Router from "vue-router";
import { Access } from "@/models/API.js";

Vue.use(Router);

const targetLocation = "warez.target";

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "login" */ "./views/Login.vue")
    },
    {
      path: "/logout",
      name: "logout",
      component: () =>
        import(/* webpackChunkName: "logout" */ "./views/Logout.vue")
    },
    {
      path: "/cart",
      name: "cart",
      component: () =>
        import(/* webpackChunkName: "logout" */ "./views/Cart.vue")
    },
    {
      path: "/",
      name: "home",
      component: () => import(/* webpackChunkName: "home" */ "./views/Home.vue")
    },
    {
      path: "/products",
      name: "ProductOverview",
      component: () =>
        import(/* webpackChunkName: "products" */ "./views/ProductOverview.vue")
    },
    {
      path: "/products/:productId",
      name: "productdetail",
      component: () =>
        import(/* webpackChunkName: "products" */ "./views/ProductDetail.vue")
    },
    {
      path: "/product/edit/:productId",
      name: "productedit",
      component: () =>
        import(/* webpackChunkName: "products" */ "./views/ProductEdit.vue")
    },
    {
      path: "/product/create",
      name: "productcreate",
      component: () =>
        import(/* webpackChunkName: "products" */ "./views/ProductCreate.vue")
    },
    {
      path: "/admin",
      name: "admin",
      component: () =>
        import(/* webpackChunkName: "products" */ "./views/Admin.vue")
    }
  ]
});

router.beforeEach((to, from, next) => {
  // Check for privileges for admin pages
  if (["admin", "productcreate", "productedit"].includes(to.name)) {
    if (!Access.isAdmin()) {
      next("/");
    }
  }

  if (Access.hasToken() && to.name === "login") {
    next("/");
  }

  if (!Access.hasToken() && to.name !== "login") {
    window.localStorage.setItem(targetLocation, to.path);
    next("/login");
  } else {
    next();
  }
});

export default router;
