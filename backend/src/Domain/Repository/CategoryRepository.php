<?php

namespace App\Domain\Repository;

use App\Domain\Model\Category;
use ReflectionException;

class CategoryRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(Category::class);
  }
}
