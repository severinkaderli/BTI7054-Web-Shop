<?php

namespace App\Domain\Repository;

use App\Database\DatabaseConnection;
use App\Domain\Model\AbstractModel;
use App\Utility\StringUtility;
use InvalidArgumentException;
use ReflectionClass;
use ReflectionException;

abstract class AbstractRepository
{
  /**
   * @var DatabaseConnection
   */
  protected $db;

  /**
   * @var string
   */
  protected $model;

  /**
   * @var string
   */
  protected $tableName;

  /**
   * @param string $model
   * @throws ReflectionException
   */
  public function __construct(string $model)
  {
    $this->db = DatabaseConnection::getInstance();
    $this->model = $model;

    $modelClass = new ReflectionClass($this->model);
    if ($modelClass->getProperty('tableName')->getValue()) {
      $this->tableName = $modelClass->getProperty('tableName')->getValue();
    } else {
      $this->tableName = lcfirst(StringUtility::camelCaseToSnakeCase($modelClass->getShortName()) . 's');
    }
  }

  /**
   * @return string
   */
  public function getModel(): string
  {
    return $this->model;
  }

  /**
   * @param string $property
   * @param $value
   * @return AbstractModel|array
   */
  public function findBy(string $property, $value)
  {
    $result = $this->findAllBy($property, $value);

    if (count($result) > 0) {
      return $result[0];
    }

    return null;
  }

  public function findAllBy(string $property, $value)
  {
    return $this->db->resultQuery(
      'SELECT * FROM ' . $this->tableName . ' WHERE ' . $property . ' = ?',
      [$value],
      $this->model
    );
  }

  /**
   * @param int $id
   * @return AbstractModel|array
   */
  public function findById(int $id)
  {
    return $this->findBy('id', $id);
  }

  /**
   * @return array
   */
  public function findAll()
  {
    return $this->db->resultQuery(
      'SELECT * FROM ' . $this->tableName,
      [],
      $this->model
    );
  }

  /**
   * Saves a new model to the database.
   *
   * @param AbstractModel $model
   * @return AbstractModel
   */
  public function save(AbstractModel $model)
  {
    $fieldValues = $this->getFieldValues($model);

    $numberOfArguments = count($fieldValues);
    $id = $this->db->insert(
      'INSERT INTO ' . $this->tableName . '(' . implode(',', array_keys($fieldValues)) . ') VALUES(' . implode(',',
        array_fill(0, $numberOfArguments, '?')) . ')',
      array_values($fieldValues)
    );
    return $this->findById($id);
  }

  /**
   * Updates a model in the database.
   *
   * @param AbstractModel $model
   * @return AbstractModel
   */
  public function update(AbstractModel $model)
  {
    $fieldValues = $this->getFieldValues($model);

    $updateStrings = array_map(function ($key) {
      return $key . ' = ?';
    }, array_keys($fieldValues));

    $id = $this->db->insert(
      'UPDATE ' . $this->tableName . ' SET ' . implode(',', $updateStrings) . ' WHERE id = ?',
      array_merge(array_values($fieldValues), [$model->getId()])
    );
    return $this->findById($id);
  }

  /**
   * Deletes a model from the database.
   *
   * @param AbstractModel $model
   */
  public function delete(AbstractModel $model)
  {
    $this->db->insert(
      'DELETE FROM ' . $this->tableName . ' WHERE id = ?',
      [$model->getId()]
    );
  }

  /**
   * @param AbstractModel $model
   * @return array
   */
  protected function getFieldValues(AbstractModel $model): array
  {
    $fieldValues = [];
    foreach ($model::$saveFields as $field) {
      $getMethod = 'get' . ucfirst($field);

      if (!method_exists($model, $getMethod)) {
        // Check for methods like 'isAdmin'
        $isMethod = 'is' . ucfirst(substr($field, 2));
        if (!method_exists($model, $isMethod)) {
          throw new InvalidArgumentException('Method ' . $getMethod . ' or ' . $isMethod . ' does not exist in ' . get_class($model));
        }
        $getMethod = $isMethod;
      }

      $value = $model->{$getMethod}();

      if ($value instanceof AbstractModel) {
        $value = $value->getId();
      }

      if (is_bool($value)) {
        $value = intval($value);
      }

      $fieldValues[StringUtility::camelCaseToSnakeCase($field)] = $value;
    }

    return $fieldValues;
  }
}
