class API {
  static async post(url, content) {
    return this.request(url, content, "POST");
  }

  static async get(url, content) {
    return this.request(url, content, "GET");
  }

  static async delete(url, content) {
    return this.request(url, content, "DELETE");
  }

  static async put(url, content) {
    return this.request(url, content, "PUT");
  }

  static async request(url, content, method, auth = true) {
    let headers = {
      Accept: "application/json",
      "Content-Type": "application/json"
    };

    if (auth) {
      headers["Authorization"] = "Bearer " + Access.getToken();
    }

    return await (
      await fetch(API.baseUrl + url, {
        method,
        headers,
        body: method === "GET" ? null : JSON.stringify(content)
      })
    ).json();
  }
}

API.baseUrl = "https://shop.api.kaderli.dev";

class Access {
  static tokenExists() {
    if (window.localStorage.getItem(Access.tokenLocation)) {
      return true;
    }
    return false;
  }

  static hasToken() {
    return !!window.localStorage.getItem(Access.tokenLocation);
  }

  static isAdmin() {
    const payload = Access.getPayload();
    if (!payload || !payload.user) {
      return false;
    }
    return payload.user.isAdmin;
  }

  static getPayload() {
    let payload = {};

    if (this.hasToken()) {
      payload = JSON.parse(atob(this.getToken().split(".")[1]));
    }

    return payload;
  }

  static async login(username, password) {
    const response = await API.request(
      "/login",
      { username, password },
      "POST",
      false
    );
    if (response.token) {
      window.localStorage.setItem(Access.tokenLocation, response.token);
      return true;
    }
    return false;
  }

  static async logout() {
    window.localStorage.removeItem(Access.tokenLocation);
  }

  static getToken() {
    return window.localStorage.getItem(Access.tokenLocation);
  }

  static updateNavigation(vm) {
    vm.$root.$children[0].fetchData();
  }
}

Access.tokenLocation = "warez.token";

export { API, Access };
