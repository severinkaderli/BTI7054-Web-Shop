<?php

namespace App\Domain\Model;

use App\Utility\StringUtility;
use DateTimeImmutable;
use Exception;
use InvalidArgumentException;
use JsonSerializable;

abstract class AbstractModel implements JsonSerializable
{
  /**
   * @var int
   */
  private $id;

  /**
   * @var DateTimeImmutable
   */
  private $createdAt;

  /**
   * @var DateTimeImmutable
   */
  private $updatedAt;

  /**
   * @var string
   */
  public static $tableName = '';

  /**
   * @var array
   */
  public static $saveFields = [];

  /**
   * @param array $data
   */
  public function __construct($data = [])
  {
    $this->fill($data);
  }

  /**
   * @return int
   */
  public function getId(): int
  {
    return $this->id;
  }

  /**
   * @param int $id
   */
  public function setId(int $id): void
  {
    $this->id = $id;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getCreatedAt(): DateTimeImmutable
  {
    return $this->createdAt;
  }

  /**
   * @param int|DateTimeImmutable $createdAt
   * @throws Exception
   */
  public function setCreatedAt($createdAt): void
  {
    if (is_string($createdAt)) {
      $createdAt = new DateTimeImmutable($createdAt);
    }

    if (!$createdAt instanceof DateTimeImmutable) {
      throw new InvalidArgumentException('createdAt');
    }

    $this->createdAt = $createdAt;
  }

  /**
   * @return DateTimeImmutable
   */
  public function getUpdatedAt(): DateTimeImmutable
  {
    return $this->updatedAt;
  }

  /**
   * @param DateTimeImmutable $updatedAt
   * @throws Exception
   */
  public function setUpdatedAt($updatedAt): void
  {
    if (is_string($updatedAt)) {
      $updatedAt = new DateTimeImmutable($updatedAt);
    }

    if (!$updatedAt instanceof DateTimeImmutable) {
      throw new InvalidArgumentException('$updatedAt');
    }

    $this->updatedAt = $updatedAt;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return [
      'id' => $this->id,
      'createdAt' => $this->createdAt->format('c'),
      'updatedAt' => $this->updatedAt->format('c')
    ];
  }

  /**
   * Fills the model with the given data.
   *
   * @param array $data
   */
  public function fill(array $data = [])
  {
    foreach ($data as $key => $value) {
      $methodName = 'set' . StringUtility::snakeCaseToCamelCase($key);
      if (method_exists($this, $methodName)) {
        $this->{$methodName}($value);
      }
    }
  }
}
