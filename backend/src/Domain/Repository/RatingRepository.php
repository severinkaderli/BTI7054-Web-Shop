<?php

namespace App\Domain\Repository;

use App\Domain\Model\AttributeType;
use App\Domain\Model\Rating;
use phpDocumentor\Reflection\Types\Integer;
use ReflectionException;

class RatingRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(Rating::class);
  }

  public function getRatingByProductId(int $productId): int
  {
    $query = "
        select round(avg(rating)) as avg_rating
        from ratings
        where product_id = ?
        group by product_id
    ";
    return $this->db->query($query, [$productId])[0]['avg_rating'] ?? 0;
  }
}
