export default {
  login: {
    title: "Login",
    username: "Username",
    password: "Password",
    login: "Login",
    error: "You could not be logged in. Double check your credentials."
  },
  home: {
    title: "Warez Electronics Store",
    subtitle: "We have warez, if you have coin.",
    leadText: `This is a small electronics store for the project in module BTI7054 at BFH.<br>
It was built by Severin Kaderli and Marius Schär.<br>
It uses Vue.js for the frontend and a REST API built in PHP for the backend.`
  },
  nav: {
    products: "Products",
    administration: "Administration"
  },
  header: {
    logout: "Logout",
    cart: "Cart"
  },
  footer: {
    created: "Created by",
    version: "Version"
  },
  overview: {
    filter: "Filter"
  },
  detail: {
    quantity: "Quantity",
    addToCart: "Add to shopping cart"
  },
  offers: "Offers",
  price: {
    was: "was"
  },
  cart: {
    title: "Shopping Cart",
    remove: "Remove item",
    clear: "Clear cart",
    empty: "Shopping Cart is empty",
    order: "Order"
  },
  admin: {
    products: "Products",
    addProduct: "Add product",
    productTable: {
      id: "ID",
      name: "Name",
      price: "Price",
      actions: "Actions",
      edit: "Edit",
      delete: "Delete"
    }
  },
  productForm: {
    edit: "Edit Product",
    create: "Create Product",
    update: "Update Product",
    save: "Save Product",
    name: "Name",
    price: "Price",
    img: "Image URL",
    isOffer: "Is Offer",
    category: "Category"
  }
};
