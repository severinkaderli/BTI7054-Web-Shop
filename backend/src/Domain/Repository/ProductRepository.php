<?php

namespace App\Domain\Repository;

use App\Domain\Model\Product;
use ReflectionException;

class ProductRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(Product::class);
  }

  /**
   * @return array
   */
  public function findOffers()
  {
    return $this->db->resultQuery(
      'SELECT * FROM ' . $this->tableName . ' WHERE is_offer=? LIMIT 5',
      [1],
      $this->model
    );
  }
}
