<?php

namespace App\Domain\Model;

class User extends AbstractModel
{
  /**
   * @var array
   */
  public static $saveFields = ['username', 'email', 'password', 'fullName', 'address', 'language', 'isAdmin'];

  /**
   * @var string
   */
  protected $username;

  /**
   * @var string
   */
  protected $email;

  /**
   * @var string
   */
  protected $password;

  /**
   * @var string
   */
  protected $fullName;

  /**
   * @var string
   */
  protected $address;

  /**
   * @var string
   */
  protected $language;

  /**
   * @var bool
   */
  protected $isAdmin;

  /**
   * @return string
   */
  public function getUsername(): string
  {
    return $this->username;
  }

  /**
   * @param string $username
   */
  public function setUsername(string $username): void
  {
    $this->username = $username;
  }

  /**
   * @return string
   */
  public function getEmail(): string
  {
    return $this->email;
  }

  /**
   * @param string $email
   */
  public function setEmail(string $email): void
  {
    $this->email = $email;
  }

  /**
   * @return string
   */
  public function getPassword(): string
  {
    return $this->password;
  }

  /**
   * @param string $password
   */
  public function setPassword(string $password): void
  {
    $this->password = $password;
  }

  /**
   * @return string
   */
  public function getFullName(): string
  {
    return $this->fullName;
  }

  /**
   * @param string $fullName
   */
  public function setFullName(string $fullName): void
  {
    $this->fullName = $fullName;
  }

  /**
   * @return string
   */
  public function getAddress(): string
  {
    return $this->address;
  }

  /**
   * @param string $address
   */
  public function setAddress(string $address): void
  {
    $this->address = $address;
  }

  /**
   * @return string
   */
  public function getLanguage(): string
  {
    return $this->language;
  }

  /**
   * @param string $language
   */
  public function setLanguage(string $language): void
  {
    $this->language = $language;
  }

  /**
   * @return bool
   */
  public function isAdmin(): bool
  {
    return $this->isAdmin;
  }

  /**
   * @param mixed $isAdmin
   */
  public function setIsAdmin($isAdmin): void
  {
    $this->isAdmin = $isAdmin;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'email' => $this->email,
        'fullName' => $this->fullName,
        'address' => $this->address,
        'language' => $this->language,
        'isAdmin' => (bool)$this->isAdmin,
      ]
    );
  }
}
