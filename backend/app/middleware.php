<?php
declare(strict_types=1);

use App\Application\Middleware\SessionMiddleware;
use Psr\Http\Message\ResponseInterface;
use Slim\App;
use Tuupola\Middleware\JwtAuthentication;
use Dotenv\Dotenv;

// Load env variables
$dotenv = Dotenv::create(dirname(__DIR__));
$dotenv->load();

return function (App $app) {
  $app->add(SessionMiddleware::class);

  // JWT Authentication middlwate
  $app->add(new JwtAuthentication([
    'path' => ['/'],
    'ignore' => ['/login'],
    'secret' => getenv('JWT_SECRET'),
    'error' => function (ResponseInterface $response, array $arguments) {
      $data = [
        'status' => 'error',
        'message' => $arguments['message']
      ];
      $response = $response->withHeader('Content-Type', 'application/json');
      $response->getBody()->write(json_encode($data));
      return $response;
    }
  ]));
};
