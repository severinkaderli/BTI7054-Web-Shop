# BTI7054-Web-Shop
This is a web shop created for the module BTI7054-Web at the BFH.

## Frontend
The frontend is created using [Vue.js](https://vuejs.org/) and the data from the
backend is accessed using a REST-API. For internationalization we use the
[vue-i18n](https://kazupon.github.io/vue-i18n/) plugin for Vue.js to make it
easier.

### Development
1. `npm install`
2. `npm run serve`

The frontend can then be accesses using `http://localhost:9000`.

## Backend
The backend is created using Slim, a micro framework written in PHP. It handles
the routing, response and request system of the API. For authentication of users
we use the [tuupola/slim-jwt-auth](https://github.com/tuupola/slim-jwt-auth)
package and for accessing settings we use the
[vlucas/phpdotenv](https://github.com/vlucas/phpdotenv) package.

As the database we use MariaDB and access the data using a  "ORM" that we
created.

### Development
1. `composer install`
2. Copy `.env.example` to `.env` and fill out the settings.
3. `php -S localhost:8080 -t public public/index.php`

The backend can then be accessed using `http://localhost:8080`.