<?php

namespace App\Application\Controller;

use App\Domain\Repository\AttributeRepository;

class AttributeController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(AttributeRepository::class);
  }
}
