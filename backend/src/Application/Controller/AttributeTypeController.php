<?php

namespace App\Application\Controller;

use App\Domain\Repository\AttributeTypeRepository;

class AttributeTypeController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(AttributeTypeRepository::class);
  }
}
