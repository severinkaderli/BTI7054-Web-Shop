<?php

namespace App\Domain\Repository;

use App\Domain\Model\AttributeType;
use ReflectionException;

class AttributeTypeRepository extends AbstractRepository
{
  /**
   * @throws ReflectionException
   */
  public function __construct()
  {
    parent::__construct(AttributeType::class);
  }
}
