const CART_CACHE_KEY = "cart";

class Cart {
  static clear() {
    window.localStorage.removeItem(CART_CACHE_KEY);
  }

  static removeProduct(product) {
    const cart = Cart.get();

    const newCart = cart.filter(entry => {
      return entry.product.id != product.id;
    });

    window.localStorage.setItem(CART_CACHE_KEY, JSON.stringify(newCart));
  }

  static add(entry) {
    Cart.removeProduct(entry.product);
    const cart = Cart.get();
    cart.push(entry);
    window.localStorage.setItem(CART_CACHE_KEY, JSON.stringify(cart));
  }

  static get() {
    let cart = window.localStorage.getItem(CART_CACHE_KEY);
    if (cart) {
      return JSON.parse(cart);
    }

    return [];
  }

  static numberOfProducts() {
    return Cart.get().length;
  }
}

export default Cart;
