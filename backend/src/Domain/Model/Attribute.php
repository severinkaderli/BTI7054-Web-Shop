<?php

namespace App\Domain\Model;

use App\Domain\Repository\AttributeTypeRepository;
use App\Domain\Repository\ProductRepository;
use App\Domain\Model\AttributeType;
use App\Domain\Model\Product;


class Attribute extends AbstractModel
{
  /**
   * @var array
   */
  public static $saveFields = ['attributeType', 'value', 'product'];

  /**
   * @var AttributeType|null
   */
  protected $attributeType;

  /**
   * @var float
   */
  protected $value;

  /**
   * @var Product|null
   */
  protected $product;

  /**
   * @param AtributeType|int|null $atributeType
   * @throws ReflectionException
   */
  public function setAttributeType($attributeType): void
  {
    if(is_numeric($attributeType)) {
      $repository = new AttributeTypeRepository();
      $attributeType = $repository->findById($attributeType);
    }

    $this->attributeType = $attributeType;
  }

  /**
   * @return AttributeType|null
   */
  public function getAttributeType(): ?AttributeType
  {
    return $this->attributeType;
  }

  /**
   * @param $value float
   */
  public function setValue(float $value): void {
    $this->value = $value;
  }

  /**
   * @return float
   */
  public function getValue(): float
  {
    return $this->value;
  }

  /**
   * @param Product|int|null $product
   * @throws ReflectionException
   */
  public function setProduct($product): void
  {
    if(is_numeric($product)) {
      $repository = new ProductRepository();
      $product = $repository->findById($product);
    }

    $this->product = $product;
  }

  /**
   * @return Product|null
   */
  public function getProduct(): ?Product
  {
    return $this->product;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'attribute_type' => $this->attributeType,
        'value' => $this->value,
        //'product' => $this->product
      ]
    );
  }
}
