<?php

namespace App\Application\Controller;

use App\Domain\Repository\RatingRepository;

class RatingController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(RatingRepository::class);
  }
}
