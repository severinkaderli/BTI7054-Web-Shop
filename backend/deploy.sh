#!/bin/bash

set -e

# Pulling recent changes
git pull

# Installing composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" > /dev/null 2>&1
php composer-setup.php > /dev/null 2>&1
php -r "unlink('composer-setup.php');" > /dev/null 2>&1

# Install composer packages
php composer.phar clearcache
php composer.phar install
