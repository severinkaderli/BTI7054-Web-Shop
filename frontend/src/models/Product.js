import { API } from "./API.js";

class Product {
  static async loadAll() {
    const result = await API.get("/products", {});

    return result;
  }
  static async loadOffers() {
    const result = await API.get("/offers", {});

    return result;
  }

  static async loadById(id) {
    const result = await API.get(`/products/${id}`, {});

    return result;
  }

  static async delete(id) {
    const result = await API.delete(`/products/${id}`, {});

    return result;
  }

  static async create(product = {}) {
    const result = await API.post("/products", product);

    return result;
  }

  static async update(product = {}) {
    const result = await API.put(`/products/${product.id}`, product);

    return result;
  }

  static async order(entries) {
    await API.post("/order", entries);
  }
}

export default Product;
