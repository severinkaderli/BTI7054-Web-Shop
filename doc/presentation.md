---
title: "Warez Electronics Store"
subtitle: "a BTI7054 project"
author:
    - Marius Schär
    - Severin Kaderli
lang: "en-UK"
toc: true
main-color: 4caf50
rule-color: 4caf50
link-color: 4caf50
glossary: true
glossary-file: glossary.tex
...

# Demo

## Demo
\LARGE [shop.kaderli.dev](https://shop.kaderli.dev)

# Design Decisions

## Vue.js
- Easy to use
- Existing knowledge

## PHP REST API
- Clean separation of Front/Backend

## Not using a CSS framework
- Vue already makes modularisation and reuse easy
- Not necessary because our CSS needs are simple
- CSS Grid

# Problems

## CORS (Cross Origin Resource Sharing)
- During frontend development
- Using the production backend shop.api.kaderli.dev

# Wall of Fame

## ORM - Seeding
```{.php}
  public static function seedDatabase()
  {
    $userRepository = new UserRepository();

    $user = new User();
    $user->setUsername('admin');
    $user->setEmail('admin@warez.shop');
    $user->setPassword(password_hash('supersecret!', PASSWORD_DEFAULT));
    $user->setFullName('Kai Brünnler');
    $user->setAddress('Bern, CH');
    $user->setIsAdmin(true);
    $user = $userRepository->save($user);
    
    // ...
  }
```

## ORM - Models
- CRUD Operations are supported without writing new DML for each model
- only DDL and the PHP Model/Controller/Repository is needed

## i18n
- Using the Vue-i18n plugin

\colsbegin \col{50}

```{.js}
// English
export default {
  login: {
    title: "Login",
    username: "Username",
    password: "Password",
    login: "Login",
    error: "You could not be logged in."
  }
}
```

\col{50}

```{.js}
// Deutsch
export default {
  login: {
    title: "Anmeldung",
    username: "Benutzername",
    password: "Passwort",
    login: "Anmelden",
    error: "Sie konnten nicht angemeldet werden."
  }
}
```

\colsend

## i18n - usage
```{.html}
<template>
  <div class="login grid">
    <div class="grid__middle">
      <h1>{{ $t("login.title") }}</h1>
      <form @submit="submit">
        <div class="input">
          <label>{{ $t("login.username") }}</label>
          <input
            type="text"
            name="username"
            placeholder="Username"
            v-model="input.username"
          />
        </div>
...
```

# Wall of Shame
## `index.php::create_db();`
- initalizes the DB via the API endpoint
- hacky, insecure

## Image Hosting
- none
- images are just hotlinked

## Database Design (for i18n)

```{.sql}
CREATE TABLE IF NOT EXISTS attribute_types (
    id INT NOT NULL AUTO_INCREMENT,
    name_en VARCHAR(255) NOT NULL, -- only english
    name_de VARCHAR(255) NOT NULL, -- and german are supported
    unit VARCHAR(255) NOT NULL,
    PRIMARY KEY(id)
);

```

# Questions

## Links

\LARGE [gitlab.com/severinkaderli/BTI7054-Web-Shop](https://gitlab.com/severinkaderli/BTI7054-Web-Shop)
