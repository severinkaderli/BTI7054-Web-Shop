<?php

namespace App\Database;

use App\Domain\Model\Category;
use App\Domain\Model\Product;
use App\Domain\Model\Rating;
use App\Domain\Model\User;
use App\Domain\Model\Attribute;
use App\Domain\Model\AttributeType;
use App\Domain\Repository\CategoryRepository;
use App\Domain\Repository\ProductRepository;
use App\Domain\Repository\RatingRepository;
use App\Domain\Repository\UserRepository;
use App\Domain\Repository\AttributeRepository;
use App\Domain\Repository\AttributeTypeRepository;
use ReflectionException;

class DatebaseUtility
{
  /**
   * Creates the database using the given SQL file.
   */
  public static function createDatabase()
  {
    $db = DatabaseConnection::getInstance();
    $query = file_get_contents(__DIR__ . '/database.sql');
    $db->rawQuery($query);
  }

  /**
   * @throws ReflectionException
   */
  public static function seedDatabase()
  {
    $userRepository = new UserRepository();
    $categoryRepository = new CategoryRepository();
    $productRepository = new ProductRepository();
    $ratingRepository = new RatingRepository();
    $attributeRepository = new AttributeRepository();
    $attributeTypeRepository = new AttributeTypeRepository();

    $userKaderli = new User();
    $userKaderli->setUsername('severinkaderli');
    $userKaderli->setEmail('severin@kaderli.dev');
    $userKaderli->setPassword(password_hash('password', PASSWORD_DEFAULT));
    $userKaderli->setFullName('Severin Kaderli');
    $userKaderli->setAddress('Musterstrasse 20, 3600 Thun');
    $userKaderli->setIsAdmin(true);
    $userKaderli->setLanguage('en');
    $userKaderli = $userRepository->save($userKaderli);

    $user = new User();
    $user->setUsername('admin');
    $user->setEmail('admin@warez.shop');
    $user->setPassword(password_hash('supersecret!', PASSWORD_DEFAULT));
    $user->setFullName('Kai Brünnler');
    $user->setAddress('Elsweyr, Skyrim');
    $user->setIsAdmin(true);
    $user->setLanguage('en');
    $user = $userRepository->save($user);

    $user = new User();
    $user->setUsername('mariusschaer');
    $user->setEmail('contact@mariusschaer.ch');
    $user->setPassword(password_hash('2', PASSWORD_DEFAULT));
    $user->setFullName('Marius Schär');
    $user->setAddress('Bill Ingaddr, Ess 3540');
    $user->setIsAdmin(false);
    $user->setLanguage('en');
    $user = $userRepository->save($user);

    $parentCategory = new Category();
    $parentCategory->setNameDe('Komponenten');
    $parentCategory->setNameEn('Components');
    $parentCategory = $categoryRepository->save($parentCategory);

    $category = new Category();
    $category->setNameDe('Speicher');
    $category->setNameEn('Storage');
    $category->setParent($parentCategory);
    $category = $categoryRepository->save($category);

    $category2 = new Category();
    $category2->setNameDe('Prozessor');
    $category2->setNameEn('CPU');
    $category2->setParent($parentCategory);
    $category2 = $categoryRepository->save($category2);

    $category3 = new Category();
    $category3->setNameDe('RAM');
    $category3->setNameEn('RAM');
    $category3->setParent($parentCategory);
    $category3 = $categoryRepository->save($category3);

    $category4 = new Category();
    $category4->setNameDe('Grafikkarte');
    $category4->setNameEn('GPU');
    $category4->setParent($parentCategory);
    $category4 = $categoryRepository->save($category4);

    $category5 = new Category();
    $category5->setNameDe('Mainboard');
    $category5->setNameEn('Mainboard');
    $category5->setParent($parentCategory);
    $category5 = $categoryRepository->save($category5);

    $category6 = new Category();
    $category6->setNameDe('Gehäuse');
    $category6->setNameEn('Cases');
    $category6->setParent($parentCategory);
    $category6 = $categoryRepository->save($category6);

    $product = new Product();
    $product->setName('WD Red');
    $product->setPrice(22000);
    $product->setIsOffer(true);
    $product->setCategory($category);
    $product->setImg('https://shop.westerndigital.com/content/dam/store/en-us/assets/products/internal-storage/wd-red-sata-hdd/gallery/wd-red-3-5-14tb.png');
    $product = $productRepository->save($product);

    $attributeType = new AttributeType();
    $attributeType->setNameDe('Kapazität');
    $attributeType->setNameEn('Capacity');
    $attributeType->setUnit('TB');
    $attributeType = $attributeTypeRepository->save($attributeType);

    $attribute = new Attribute();
    $attribute->setAttributeType($attributeType);
    $attribute->setValue(2);
    $attribute->setProduct($product);
    $attribute = $attributeRepository->save($attribute);

    $attribute = new Attribute();
    $attribute->setAttributeType($attributeType);
    $attribute->setValue(4);
    $attribute->setProduct($product);
    $attribute = $attributeRepository->save($attribute);

    $attribute = new Attribute();
    $attribute->setAttributeType($attributeType);
    $attribute->setValue(8);
    $attribute->setProduct($product);
    $attribute = $attributeRepository->save($attribute);

    $rating = new Rating();
    $rating->setProductId($product->getId());
    $rating->setRating(3);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);

    $rating = new Rating();
    $rating->setProductId($product->getId());
    $rating->setRating(1);
    $rating->setUserId($userKaderli->getId());
    $ratingRepository->save($rating);

    $product2 = new Product();
    $product2->setName('WD Blue');
    $product2->setPrice(4200);
    $product2->setIsOffer(true);
    $product2->setCategory($category);
    $product2->setImg('https://shop.westerndigital.com/content/dam/store/en-us/assets/products/internal-storage/wd-blue-mobile-sata-hdd/gallery/wd-blue-mobile-1tb.png.thumb.1280.1280.png');
    $product2 = $productRepository->save($product2);

    $rating = new Rating();
    $rating->setProductId($product2->getId());
    $rating->setRating(4);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);

    $product3 = new Product();
    $product3->setName('Ryzen 3950X');
    $product3->setPrice(86900);
    $product3->setIsOffer(false);
    $product3->setCategory($category2);
    $product3->setImg('https://c1.neweggimages.com/ProductImage/19-113-616-Z01.jpg');
    $product3 = $productRepository->save($product3);

    $rating = new Rating();
    $rating->setProductId($product3->getId());
    $rating->setRating(5);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);

    $product4 = new Product();
    $product4->setName('Corsair Vengeance RGB Pro');
    $product4->setPrice(16700);
    $product4->setIsOffer(false);
    $product4->setCategory($category3);
    $product4->setImg('https://static.digitecgalaxus.ch/Files/1/8/7/4/1/9/0/3/null-H-002.xxl3.jpgexportGa4PCo68TlLe9g?fit=inside%7C708:330&output-format=progressive-jpeg');
    $product4 = $productRepository->save($product4);

    $rating = new Rating();
    $rating->setProductId($product4->getId());
    $rating->setRating(4);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);

    $product5 = new Product();
    $product5->setName('ASUS GeForce RTX 2080S ROG STRIX O8G');
    $product5->setPrice(90900);
    $product5->setIsOffer(false);
    $product5->setCategory($category4);
    $product5->setImg('https://static.digitecgalaxus.ch/Files/2/6/9/4/6/6/1/7/ROG-STRIX-RTX2080S_2D.jpg?fit=inside%7C708:330&output-format=progressive-jpeg');
    $product5 = $productRepository->save($product5);

    $rating = new Rating();
    $rating->setProductId($product5->getId());
    $rating->setRating(5);
    $rating->setUserId($user->getId());

    $product6 = new Product();
    $product6->setName('Gigabyte B450 Aorus Elite');
    $product6->setPrice(11300);
    $product6->setIsOffer(false);
    $product6->setCategory($category5);
    $product6->setImg('https://static.digitecgalaxus.ch/Files/1/7/6/6/1/3/7/5/2018071815374883_big.jpg?fit=inside%7C708:330&output-format=progressive-jpeg');
    $product6 = $productRepository->save($product6);

    $rating = new Rating();
    $rating->setProductId($product6->getId());
    $rating->setRating(2);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);

    $product7 = new Product();
    $product7->setName('Corsair Obsidian 500D RGB');
    $product7->setPrice(27100);
    $product7->setIsOffer(false);
    $product7->setCategory($category6);
    $product7->setImg('https://static.digitecgalaxus.ch/Files/1/4/5/8/7/8/0/7/500D_SE_02.png?fit=inside%7C708:330&output-format=png');
    $product7 = $productRepository->save($product7);

    $rating = new Rating();
    $rating->setProductId($product7->getId());
    $rating->setRating(3);
    $rating->setUserId($user->getId());
    $ratingRepository->save($rating);
  }
}
