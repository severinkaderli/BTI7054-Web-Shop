<?php

namespace App\Application\Controller;

use App\Domain\Repository\ProductRepository;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

class ProductController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(ProductRepository::class);
  }

  /**
   * @param Request $request
   * @param Response $response
   * @param array $arguments
   * @return Response
   */
  public function offers(Request $request, Response $response, array $arguments = [])
  {
    return $this->jsonResponse($response, $this->repository->findOffers());
  }

  public function order(Request $request, Response $response, array $arguments = [])
  {
    $arguments = json_decode($request->getBody()->getContents());

    $date = new \DateTime();
    $subject = 'Ihre Bestellung vom ' . $date->format('Y-m-d H:i:s');
    $body = "<html><body>Sie haben folgendes bestellt:<br><br><table></table>";
    foreach($arguments as $entry) {
      $body .= "<tr>";
      $body .= "<td>" . $entry->product->name . "</td><td>x" . $entry->quantity . "</td><td align=\"right\">" . $this->getPrice($entry->product->price, $entry->quantity) . "</td>";
      $body .= "</tr>";
    }
    $body .= "</table><br>Vielen Dank für Ihren Einkauf<br>Ihr Warez-Team</body></html>";

    $headers = [
      'Content-type: text/html',
      'From: Warez-Team <noreply@warez.shop>'
    ];
    $headers = implode("\r\n", $headers);

    mail($request->getAttribute('token')['user']->email, $subject, $body, $headers);

    return $this->jsonResponse($response, []);
  }

  private function getPrice($price, $quantity): string
  {
    return number_format($quantity * $price / 100, 2, ",", "'") . " CHF";
  }
}
