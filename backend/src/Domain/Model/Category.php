<?php

namespace App\Domain\Model;

use App\Domain\Repository\CategoryRepository;
use ReflectionException;

class Category extends AbstractModel
{
  /**
   * @var string
   */
  public static $tableName = 'categories';

  /**
   * @var array
   */
  public static $saveFields = ['nameDe', 'nameEn', 'parent'];

  /**
   * @var
   */
  protected $nameDe;

  /**
   * @var string
   */
  protected $nameEn;

  /**
   * @var null|Category
   */
  protected $parent;


  /**
   * @return mixed
   */
  public function getNameDe()
  {
    return $this->nameDe;
  }

  /**
   * @param mixed $nameDe
   */
  public function setNameDe($nameDe): void
  {
    $this->nameDe = $nameDe;
  }

  /**
   * @return string
   */
  public function getNameEn(): string
  {
    return $this->nameEn;
  }

  /**
   * @param string $nameEn
   */
  public function setNameEn(string $nameEn): void
  {
    $this->nameEn = $nameEn;
  }

  /**
   * @return Category|null
   */
  public function getParent(): ?Category
  {
    return $this->parent;
  }

  /**
   * @param Category|int|null $parent
   * @throws ReflectionException
   */
  public function setParent($parent): void
  {
    if(is_numeric($parent)) {
      $categoryRepository = new CategoryRepository();
      $parent = $categoryRepository->findById($parent);
    }

    $this->parent = $parent;
  }


  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'nameDe' => $this->nameDe,
        'nameEn' => $this->nameEn,
        'parent' => $this->parent,
      ]
    );
  }

}
