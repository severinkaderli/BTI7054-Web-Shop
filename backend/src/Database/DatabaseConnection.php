<?php

namespace App\Database;

use App\Domain\Model\AbstractModel;
use Dotenv\Dotenv;
use PDO;

class DatabaseConnection
{
  private static $instance = null;
  /**
   * @var PDO
   */
  private $pdo = null;

  private function __construct()
  {
    $dotenv = Dotenv::create(dirname(dirname(__DIR__)));
    $dotenv->load();
    $this->pdo = new PDO(
      sprintf(
        "mysql:host=%s;dbname=%s",
        getenv('DB_HOST'),
        getenv('DB_NAME')
      ),
      getenv('DB_USER'),
      getenv('DB_PASS')
    );
    $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  }

  /**
   * @return DatabaseConnection
   */
  public static function getInstance()
  {
    if (!self::$instance) {
      self::$instance = new DatabaseConnection();
    }

    return self::$instance;
  }

  /**
   * @param string $query
   * @param array $arguments
   * @param string $class
   * @return array
   */
  public function resultQuery(string $query, array $arguments, string $class = AbstractModel::class)
  {
    $models = [];
    $statement = $this->pdo->prepare($query);
    $statement->execute($arguments);

    while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
      $models[] = new $class($row);
    }

    return $models;
  }

  public function insert(string $query, array $arguments): int
  {
    $statement = $this->pdo->prepare($query);
    $statement->execute($arguments);
    return $this->pdo->lastInsertId();
  }

  public function query(string $query, array $arguments): array
  {
    $statement = $this->pdo->prepare($query);
    $statement->execute($arguments);
    return $statement->fetchAll();
  }

  public function rawQuery(string $query)
  {
    $this->pdo->exec($query);
  }
}
