const webpack = require("webpack");
const childProcess = require("child_process");

const commitHash = childProcess
  .execSync("git rev-parse --short HEAD")
  .toString();

module.exports = {
  publicPath: "/",
  devServer: {
    port: 9000
  },
  configureWebpack: {
    plugins: [
      new webpack.DefinePlugin({
        VERSION: JSON.stringify(require("./package.json").version),
        COMMIT_HASH: JSON.stringify(commitHash)
      })
    ]
  }
};
