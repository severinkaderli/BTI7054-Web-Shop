export default {
  login: {
    title: "Anmeldung",
    username: "Benutzername",
    password: "Passwort",
    login: "Anmelden",
    error:
      "Sie konnten nicht angemeldet werden. Bitte überprüfen Sie Ihre Eingaben."
  },
  home: {
    title: "Warez Elektronikladen",
    subtitle: "Wir haben Ware, wenn Sie Münzen haben.",
    leadText: `Dies ist ein kleiner Elektronikladen für das Modul BTI7054 an der BFH.<br>
Er wurde erstellt von Severin Kaderli und Marius Schär.<br>
Er braucht Vue.js für das Frontend und eine REST-API in PHP für das Backend.`
  },
  nav: {
    products: "Produkte",
    administration: "Verwaltung"
  },
  header: {
    logout: "Abmelden",
    cart: "Einkaufswagen"
  },
  footer: {
    created: "Erstellt von",
    version: "Version"
  },
  overview: {
    filter: "Filter"
  },
  detail: {
    quantity: "Menge",
    addToCart: "Zu Einkaufswagen hinzufügen"
  },
  offers: "Sonderangebote",
  price: {
    was: "statt"
  },
  cart: {
    title: "Einkaufswagen",
    remove: "Produkt entfernen",
    clear: "Einkaufswagen leeren",
    empty: "Einkaufswagen ist leer",
    order: "Bestellung abschicken"
  },
  admin: {
    products: "Produkte",
    addProduct: "Produkt hinzufügen",
    productTable: {
      id: "ID",
      name: "Name",
      price: "Preis",
      actions: "Aktionen",
      edit: "Bearbeiten",
      delete: "Löschen"
    }
  },
  productForm: {
    edit: "Produkt bearbeiten",
    create: "Produkt erstellen",
    update: "Produkt aktualisieren",
    save: "Produkt hinzufügen",
    name: "Name",
    price: "Preis",
    img: "Bild-URL",
    isOffer: "Ist Angebot",
    category: "Kategorie"
  }
};
