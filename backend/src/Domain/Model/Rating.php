<?php

namespace App\Domain\Model;

class Rating extends AbstractModel
{
  /**
   * @var array
   */
  public static $saveFields = ['rating', 'productId', 'userId'];

  /**
   * @var int
   */
  protected $rating;

  /**
   * @var int
   */
  protected $productId;

  /**
   * @var int
   */
  protected $userId;

  /**
   * @return int
   */
  public function getRating(): int
  {
    return $this->rating;
  }

  /**
   * @return int
   */
  public function getProductId(): int
  {
    return $this->productId;
  }

  /**
   * @return int
   */
  public function getUserId(): int
  {
    return $this->userId;
  }

  /**
   * @param int $rating
   */
  public function setRating(int $rating): void
  {
    $this->rating = $rating;
  }

  /**
   * @param int $productId
   */
  public function setProductId(int $productId): void
  {
    $this->productId = $productId;
  }

  /**
   * @param int $userId
   */
  public function setUserId(int $userId): void
  {
    $this->userId = $userId;
  }

  /**
   * @inheritDoc
   */
  public function jsonSerialize()
  {
    return array_merge(
      parent::jsonSerialize(),
      [
        'rating' => $this->rating,
        'user_id' => $this->userId,
        'product_id' => $this->productId
      ]
    );
  }
}
