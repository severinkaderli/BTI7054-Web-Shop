<?php

namespace App\Application\Controller;

use App\Domain\Repository\CategoryRepository;

class CategoryController extends AbstractController
{
  public function __construct()
  {
    parent::__construct(CategoryRepository::class);
  }
}
