<?php
declare(strict_types=1);

use App\Application\Controller\AttributeController;
use App\Application\Controller\AttributeTypeController;
use App\Application\Controller\CategoryController;
use App\Application\Controller\ProductController;
use App\Application\Controller\RatingController;
use App\Application\Controller\UserController;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

return function (App $app) {
  // Ratings
  $app->group('/ratings', function (RouteCollectorProxy $group) {
    $group->get('', RatingController::class . ':index');
    $group->get('/{id}', RatingController::class . ':get');
    $group->post('', RatingController::class . ':new');
    $group->put('/{id}', RatingController::class . ':update');
    $group->delete('/{id}', RatingController::class . ':delete');
  });

  // Attributes
  $app->group('/attributes', function (RouteCollectorProxy $group) {
    $group->get('', AttributeController::class . ':index');
    $group->get('/{id}', AttributeController::class . ':get');
    $group->post('', AttributeController::class . ':new');
    $group->put('/{id}', AttributeController::class . ':update');
    $group->delete('/{id}', AttributeController::class . ':delete');
  });

  // AttributeTypes
  $app->group('/attributetypes', function (RouteCollectorProxy $group) {
    $group->get('', AttributeTypeController::class . ':index');
    $group->get('/{id}', AttributeTypeController::class . ':get');
    $group->post('', AttributeTypeController::class . ':new');
    $group->put('/{id}', AttributeTypeController::class . ':update');
    $group->delete('/{id}', AttributeTypeController::class . ':delete');
  });

  // Products
  $app->group('/products', function (RouteCollectorProxy $group) {
    $group->get('', ProductController::class . ':index');
    $group->get('/{id}', ProductController::class . ':get');
    $group->post('', ProductController::class . ':new');
    $group->put('/{id}', ProductController::class . ':update');
    $group->delete('/{id}', ProductController::class . ':delete');
  });

  $app->post('/order', ProductController::class . ':order');
  $app->get('/offers', ProductController::class . ':offers');

  // Categories
  $app->group('/categories', function (RouteCollectorProxy $group) {
    $group->get('', CategoryController::class . ':index');
    $group->get('/{id}', CategoryController::class . ':get');
    $group->post('', CategoryController::class . ':new');
    $group->put('/{id}', CategoryController::class . ':update');
    $group->delete('/{id}', CategoryController::class . ':delete');
  });

  // Users
  $app->group('/users', function (RouteCollectorProxy $group) {
    $group->get('', UserController::class . ':index');
    $group->get('/{id}', UserController::class . ':get');
    $group->post('', UserController::class . ':new');
    $group->put('/{id}', UserController::class . ':update');
    $group->delete('/{id}', CategoryController::class . ':delete');
  });
  $app->post('/login', UserController::class . ':login');
};
