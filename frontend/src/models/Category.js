import { API } from "./API.js";

class Category {
  static async loadAll() {
    const result = await API.get("/categories", {});

    return result;
  }

  static async loadById(id) {
    const result = await API.get("/categories/" + id, {});

    return result;
  }

  static getCategories(category) {
    let categories = [];
    while (category) {
      categories.push(category);
      category = category.parent ? category.parent : null;
    }
    return categories.reverse();
  }
}

export default Category;
